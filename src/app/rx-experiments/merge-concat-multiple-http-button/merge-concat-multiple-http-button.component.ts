import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {mergeAll, mergeMap, reduce, tap} from "rxjs/operators";
import {concat, from, merge, of} from "rxjs";

@Component({
  selector: 'app-merge-concat-multiple-http-button',
  templateUrl: './merge-concat-multiple-http-button.component.html',
  styleUrls: ['./merge-concat-multiple-http-button.component.css']
})
export class MergeConcatMultipleHttpButtonComponent implements OnInit {

  @Input()
  url: string;
  @Input()
  label: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  loadStuff() {
    concat(
      of([`http://localhost:3000/example/start`]),
      merge(
      this.http.get(`${this.url}/1`),
      this.http.get( `${this.url}/2`)),
      of([`http://localhost:3000/example/stop`]))
      .pipe(
       // tap(resp => {
       //   console.log('tap', resp);
       // })
    //   ,
        mergeMap((resp: Array<string>) => {
         return resp.map(url => this.http.get(url));
       }),
       mergeAll(),
      // reduce((acc, value) => acc.concat(value), []),
  ).subscribe(console.log, console.error);
  }
}
