import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-push-button',
  templateUrl: './push-button.component.html',
  styleUrls: ['../style.css']
})
export class PushButtonComponent implements OnInit {

  @Output()
  valueChanged = new EventEmitter();
  count = 0;

  constructor() {
  }

  ngOnInit() {
  }

  clicked() {
    this.count += 1;
    this.valueChanged.emit(this.count);
  }
}
