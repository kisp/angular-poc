import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-detect-push',
  templateUrl: './detect-push.component.html',
  styleUrls: ['./detect-push.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetectPushComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
