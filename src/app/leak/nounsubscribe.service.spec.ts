import { TestBed } from '@angular/core/testing';

import { NounsubscribeService } from './nounsubscribe.service';

describe('NounsubscribeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NounsubscribeService = TestBed.get(NounsubscribeService);
    expect(service).toBeTruthy();
  });
});
