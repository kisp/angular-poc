import {Injectable} from '@angular/core';
import {BehaviorSubject, interval, Observable} from 'rxjs';

declare global {
  interface Performance {
    readonly memory: any;
  }
}

export interface MemoryInfo {
  max: number;
  total: number;
  used: number;
}

@Injectable({
  providedIn: 'root'
})
export class MemoryInfoProviderService {

  private displayHighFreq$ = interval(100);

  private info: BehaviorSubject<MemoryInfo>;

  constructor() {
    this.info = new BehaviorSubject<MemoryInfo>({max: 0, total: 0, used: 0});
    this.displayHighFreq$.subscribe(() => this.fetchInfo());
  }

  public getValues$(): Observable<MemoryInfo> {
    return this.info.asObservable();
  }

  private fetchInfo() {
    const {
      jsHeapSizeLimit = 0,
      totalJSHeapSize = 0,
      usedJSHeapSize = 0
    } = window.performance.memory || {};

    this.info.next({
      max: jsHeapSizeLimit,
      total: totalJSHeapSize,
      used: usedJSHeapSize
    });
  }

}
