import { TestBed } from '@angular/core/testing';

import { MemoryInfoProviderService } from './memory-info-provider.service';

describe('MemoryInfoProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MemoryInfoProviderService = TestBed.get(MemoryInfoProviderService);
    expect(service).toBeTruthy();
  });
});
