import { TestBed } from '@angular/core/testing';

import { ListDataSourceService } from './list-data-source.service';

describe('ListDataSourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListDataSourceService = TestBed.get(ListDataSourceService);
    expect(service).toBeTruthy();
  });
});
