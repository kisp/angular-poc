import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-container-default',
  templateUrl: './container-default.component.html',
  styleUrls: ['../style.css']
})
export class ContainerDefaultComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
