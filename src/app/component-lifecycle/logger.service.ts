import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  private events = new BehaviorSubject("None");

  constructor() { }

  getEvents$(): Observable<string> {
    return this.events.asObservable();
  }

  log(...str: string[]) {
    const timeStamp = new Date();
    const logLine = [timeStamp.toLocaleTimeString(), '=>', ...str];
    this.events.next(logLine.join(' '));
  }
}
