import {Injectable} from '@angular/core';
import {IListItem, ListItem} from './item/ListItem';
import {BehaviorSubject, interval, Observable, Subject} from 'rxjs';

const NUMBERS = 'one,two,three,four,five,six,seven,eight,nine,ten'.split(',');

@Injectable({
  providedIn: 'root'
})
export class ListDataSourceService {

  private listItems: IListItem[];
  private interval$ = interval(1000);
  private newlist: Subject<IListItem[]>;

  constructor() {
    this.listItems = new Array<IListItem>();

    for (let i = 0; i < 10; i++) {
      this.listItems.push(new ListItem(`${NUMBERS[i]}`, `${i}`));
    }

    this.newlist = new BehaviorSubject(this.listItems);

    this.interval$.subscribe(this.changeValue.bind(this));
  }

  getItems(): Observable<IListItem[]> {
    return this.newlist.asObservable();
  }

  changeValue(nextNumber: number) {
    const selectedItem = this.listItems[nextNumber % this.listItems.length];
    selectedItem.text = String(Number(selectedItem.text) + 1);
    const newArr = [];
    // This is important! all the items are immutable this way to demonstrate the refresh
    this.listItems.forEach(item => {
      newArr.push({...item});
    });
    this.listItems = newArr;

    this.newlist.next(this.listItems);
  }
}
