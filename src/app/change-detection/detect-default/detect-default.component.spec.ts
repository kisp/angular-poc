import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetectDefaultComponent } from './detect-default.component';

describe('DetectDefaultComponent', () => {
  let component: DetectDefaultComponent;
  let fixture: ComponentFixture<DetectDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetectDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetectDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
