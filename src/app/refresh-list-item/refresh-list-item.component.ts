import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IListItem} from './item/ListItem';
import {ListDataSourceService} from './list-data-source.service';
import {Subscription} from 'rxjs';
import {LogDomChangesService} from "./log-dom-changes.service";

@Component({
  selector: 'app-refresh-list-item',
  templateUrl: './refresh-list-item.component.html',
  styleUrls: ['./refresh-list-item.component.css']
})
export class RefreshListItemComponent implements OnInit, OnDestroy {
  listItems: IListItem[];

  private subscription: Subscription;

  @ViewChild('list', {static: true})
  list;

  constructor(private ds: ListDataSourceService, private log: LogDomChangesService) {
  }

  ngOnInit() {
    this.subscription =
      this.ds.getItems().subscribe(items => {
        this.listItems = items;
      });
    this.subscription.add( this.log.logChanges(this.list.nativeElement));
  }

  getIndex(index, item) {
    //return 1;
    return item && item.id;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
