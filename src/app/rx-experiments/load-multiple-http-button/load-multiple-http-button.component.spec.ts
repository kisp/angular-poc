import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadMultipleHttpButtonComponent } from './load-multiple-http-button.component';

describe('LoadMultipleHttpButtonComponent', () => {
  let component: LoadMultipleHttpButtonComponent;
  let fixture: ComponentFixture<LoadMultipleHttpButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadMultipleHttpButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadMultipleHttpButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
