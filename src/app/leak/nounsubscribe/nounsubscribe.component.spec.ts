import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NounsubscribeComponent } from './nounsubscribe.component';

describe('NounsubscribeComponent', () => {
  let component: NounsubscribeComponent;
  let fixture: ComponentFixture<NounsubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NounsubscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NounsubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
