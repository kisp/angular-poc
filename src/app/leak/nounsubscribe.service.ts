import {Injectable} from '@angular/core';
import {BehaviorSubject, interval, Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NounsubscribeService {

  private sub = interval(10000);
  private subCount = 0;
  private subScriptions: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor() {
  }

  getSub(): Observable<number> {
    this.subScriptions.next(++this.subCount);
    return this.sub;
  }

  getSubCount(): Observable<number> {
    return this.subScriptions.asObservable();
  }
}
