import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Subscription} from 'rxjs';
import {MemoryChartDataProviderService} from '../memory-chart-data-provider.service';

@Component({
  selector: 'app-memory-chart',
  templateUrl: './memory-chart.component.html',
  styleUrls: ['./memory-chart.component.css']
})
export class MemoryChartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription;

  chartData: any;

  constructor(private dataProvider: MemoryChartDataProviderService) {
    this.subscriptions = this.dataProvider.getChartData$().subscribe((d) => this.chartData = d);
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
