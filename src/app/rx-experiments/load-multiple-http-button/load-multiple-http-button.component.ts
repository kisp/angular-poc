import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {mergeAll, mergeMap, reduce, tap} from 'rxjs/operators';

@Component({
  selector: 'app-load-multiple-http-button',
  templateUrl: './load-multiple-http-button.component.html'
})
export class LoadMultipleHttpButtonComponent implements OnInit {

  @Input()
  url: string;
  @Input()
  label: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  loadStuff() {
    this.http.get(this.url).pipe(
      mergeMap((resp: Array<string>) => {
        return resp.map(url => this.http.get(url));
      }),
      mergeAll(),
      reduce((acc, value) => acc.concat(value), []),
      tap(resp => {
        console.log('tap', resp);
      })
    ).subscribe(console.log, console.error);
  }
}
