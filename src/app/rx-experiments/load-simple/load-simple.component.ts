import {Component, Input, OnInit} from '@angular/core';
import {mergeAll, mergeMap, reduce, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-load-simple',
  templateUrl: './load-simple.component.html',
  styleUrls: ['./load-simple.component.css']
})
export class LoadSimpleComponent implements OnInit {

  @Input()
  url: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }


  loadStuff() {
    this.http.get(this.url).subscribe(console.log, console.error);
  }
}
