import { TestBed } from '@angular/core/testing';

import { LogDomChangesService } from './log-dom-changes.service';

describe('LogDomChangesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogDomChangesService = TestBed.get(LogDomChangesService);
    expect(service).toBeTruthy();
  });
});
