import {Injectable} from '@angular/core';
import {BehaviorSubject, interval, Observable} from 'rxjs';
import {MemoryInfo, MemoryInfoProviderService} from './memory-info-provider.service';

@Injectable({
  providedIn: 'root'
})
export class MemoryChartDataProviderService {

  private chartIndex = 0;
  private chartData = {
    labels: [],
    datasets: [
      {
        label: 'Total',
        data: [],
        fill: false
      },
      {
        label: 'Used',
        data: [],
        fill: true,
        borderColor: '#4bc0c0'
      }
    ]
  };
  private chartData$: BehaviorSubject<any>;
  displayLowFreq$ = interval(5000);

  constructor(private memoryInfo: MemoryInfoProviderService) {
    this.chartData$ = new BehaviorSubject<any>(this.chartData);
    this.displayLowFreq$.subscribe(() => this.displayChart());
    this.memoryInfo.getValues$().subscribe((info: MemoryInfo) => this.pushData(info.total, info.used));
  }

  displayChart() {
    this.chartData$.next({...this.chartData});
  }

  private pushData(totalJSHeapSize, usedJSHeapSize) {
    this.chartData.labels.push(++this.chartIndex);
    this.chartData.datasets[0].data.push(totalJSHeapSize);
    this.chartData.datasets[1].data.push(usedJSHeapSize);

    if (this.chartData.datasets[0].data.length > 500) {
      this.chartData.labels.shift();
      this.chartData.datasets[0].data.shift();
      this.chartData.datasets[1].data.shift();
    }
  }

  getChartData$(): Observable<any> {
    return this.chartData$.asObservable();
  }

}
