import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsComponent } from './rx-experiments/buttons.component';
import { Routes } from '@angular/router';
import { EventSourceComponent } from './sse/event-source/event-source.component';
import { ContainerComponent } from './change-detection/container/container.component';
import { NounsubscribeComponent } from './leak/nounsubscribe/nounsubscribe.component';
import { QueryParamsComponent } from "./router/query-params/query-params.component";
import { RefreshListItemComponent } from "./refresh-list-item/refresh-list-item.component";
import { ChangingAsyncArrayPlusCdComponent } from './changing-async-array-plus-cd/changing-async-array-plus-cd.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AppRoutingModule { }


export const routes: Routes = [
  { path: 'buttons', component: ButtonsComponent},
  { path: 'sse', component: EventSourceComponent},
  { path: 'change-detection', component: ContainerComponent},
  { path: 'leak', component: NounsubscribeComponent},
  { path: 'router/:id/entities/:entityId', component: QueryParamsComponent},
  { path: 'list-item-refresh', component: RefreshListItemComponent},
  { path: 'async-arr-cd', component: ChangingAsyncArrayPlusCdComponent}
];
