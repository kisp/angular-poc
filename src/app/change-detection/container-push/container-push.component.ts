import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-container-push',
  templateUrl: './container-push.component.html',
  styleUrls: ['../style.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerPushComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
