import { Component, OnInit } from '@angular/core';
import {EventSourcePolyfill} from 'ng-event-source';

@Component({
  selector: 'app-event-source',
  templateUrl: './event-source.component.html',
  styleUrls: ['./event-source.component.css']
})
export class EventSourceComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    //EventSourcePolyfill
  }

}
