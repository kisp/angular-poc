import {ChangeDetectorRef, Component, ElementRef, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {NounsubscribeService} from '../nounsubscribe.service';
import {tap} from 'rxjs/operators';
import {LoggerService} from '../../component-lifecycle/logger.service';
import {interval} from 'rxjs';
import {MemoryInfo, MemoryInfoProviderService} from '../memory-info-provider.service';


@Component({
  selector: 'app-nounsubscribe',
  templateUrl: './nounsubscribe.component.html',
  styleUrls: ['./nounsubscribe.component.css']
})
export class NounsubscribeComponent implements OnInit {
  subCount$;
  max: number;
  total: number;
  used: number;

  usedStr: string;

  constructor(private nounsub: NounsubscribeService, private logger: LoggerService, private memoryInfo: MemoryInfoProviderService) {
  }

  ngOnInit() {
    this.memoryInfo.getValues$().subscribe((info) => this.displayMem(info));

    this.subCount$ = this.nounsub.getSubCount();

    this.nounsub.getSub().pipe(
      tap(console.log)
    ).subscribe();
  }

  loadStuff() {
    for (let i = 0; i < 5000; i++) {
      this.nounsub.getSub().pipe(
        tap(() => {
        })
      ).subscribe();
    }
  }

  displayMem(info:MemoryInfo) {
    this.max = info.max;
    this.total = info.total;
    this.used = info.used;

    this.usedStr = `${Math.floor(info.used / 1000000)} M`;
  }

}
