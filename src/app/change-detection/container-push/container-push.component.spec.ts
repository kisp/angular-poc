import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerPushComponent } from './container-push.component';

describe('ContainerPushComponent', () => {
  let component: ContainerPushComponent;
  let fixture: ComponentFixture<ContainerPushComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerPushComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerPushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
