import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetectPushComponent } from './detect-push.component';

describe('DetectPushComponent', () => {
  let component: DetectPushComponent;
  let fixture: ComponentFixture<DetectPushComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetectPushComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetectPushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
