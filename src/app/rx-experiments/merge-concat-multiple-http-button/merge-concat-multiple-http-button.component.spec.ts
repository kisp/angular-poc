import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeConcatMultipleHttpButtonComponent } from './merge-concat-multiple-http-button.component';

describe('MergeConcatMultipleHttpButtonComponent', () => {
  let component: MergeConcatMultipleHttpButtonComponent;
  let fixture: ComponentFixture<MergeConcatMultipleHttpButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeConcatMultipleHttpButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeConcatMultipleHttpButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
