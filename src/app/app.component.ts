import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { NavigationEnd, Router } from '@angular/router';
import { LoggerService } from './component-lifecycle/logger.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-poc';
  menuItems: MenuItem[];

  constructor(private router: Router, private logger: LoggerService) {
  }

  ngOnInit() {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.logger.log('Navigation', 'url', e.url);

        this.menuItems = this.menuItems.map(menuItem => ({
          ...menuItem,
          styleClass: menuItem.routerLink === e.url ? 'activeMenuItem' : ''
        }));
      }
    });

    this.menuItems = [
      {label: 'Rx test buttons', icon: 'pi pi-fw pi-sitemap', routerLink: '/buttons'},
      {label: 'Charts', icon: 'pi pi-fw pi-chart-bar', routerLink: '/charts'},
      {label: 'Main', icon: 'pi pi-fw pi-home', routerLink: '/'},
      {label: 'Server-sent events ', icon: 'pi pi-fw pi-angle-double-up', routerLink: '/sse'},
      {label: 'Change detection', icon: 'pi pi-fw pi-refresh', routerLink: '/change-detection'},
      {label: 'Leak (Chrome)', routerLink: '/leak'},
      {label: 'QueryParam', routerLink: 'router/1/entities/2', queryParams: {a: 'a', b: 2, c: 'Cecil'}},
      {label: 'List item refresh', routerLink: 'list-item-refresh'},
      {label: 'Async arr plus cd', routerLink: 'async-arr-cd'}
      //{label: 'Lifecycle events'}
    ];
  }
}
