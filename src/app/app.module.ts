import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RxExperimentsModule } from './rx-experiments/rx-experiments.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule, routes } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { MenuModule } from 'primeng/menu';
import { EventSourceComponent } from './sse/event-source/event-source.component';
import { ContainerComponent } from './change-detection/container/container.component';
import { DisplayCountComponent } from './change-detection/display-count/display-count.component';
import { PushButtonComponent } from './change-detection/push-button/push-button.component';
import { DetectDefaultComponent } from './change-detection/detect-default/detect-default.component';
import { DetectPushComponent } from './change-detection/detect-push/detect-push.component';
import { ContainerPushComponent } from './change-detection/container-push/container-push.component';
import { ContainerDefaultComponent } from './change-detection/container-default/container-default.component';
import { ButtonModule } from 'primeng/button';
import { BoundButtonComponent } from './change-detection/bound-button/bound-button.component';
import { LoggerComponent } from './component-lifecycle/logger/logger.component';
import { FormsModule } from '@angular/forms';
import { NounsubscribeComponent } from './leak/nounsubscribe/nounsubscribe.component';
import { ChartModule } from 'primeng/chart';
import { MemoryChartComponent } from './leak/memory-chart/memory-chart.component';
import { QueryParamsComponent } from './router/query-params/query-params.component';
import { RefreshListItemComponent } from './refresh-list-item/refresh-list-item.component';
import { ItemComponent } from './refresh-list-item/item/item.component';
import { ShowDomChangesDirective } from './refresh-list-item/show-dom-changes.directive';
import { ChangingAsyncArrayPlusCdComponent } from './changing-async-array-plus-cd/changing-async-array-plus-cd.component';

@NgModule({
  declarations: [
    AppComponent,
    EventSourceComponent,
    ContainerComponent,
    DisplayCountComponent,
    PushButtonComponent,
    DetectDefaultComponent,
    DetectPushComponent,
    ContainerPushComponent,
    ContainerDefaultComponent,
    BoundButtonComponent,
    LoggerComponent,
    NounsubscribeComponent,
    MemoryChartComponent,
    QueryParamsComponent,
    RefreshListItemComponent,
    ItemComponent,
    ShowDomChangesDirective,
    ChangingAsyncArrayPlusCdComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RxExperimentsModule,
    AppRoutingModule,
    MenuModule,
    RouterModule.forRoot(routes),
    ButtonModule,
    FormsModule,
    ChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
