import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefreshListItemComponent } from './refresh-list-item.component';

describe('RefreshListItemComponent', () => {
  let component: RefreshListItemComponent;
  let fixture: ComponentFixture<RefreshListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefreshListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefreshListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
