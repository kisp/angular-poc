import {EventEmitter, Injectable} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {LoggerService} from '../component-lifecycle/logger.service';

@Injectable({
  providedIn: 'root'
})
export class LogDomChangesService {

  private changes: MutationObserver;
  private domChange = new EventEmitter<MutationRecord>();

  constructor(private logger: LoggerService) {
    this.changes = new MutationObserver((mutations => {
      mutations.forEach((mutation: MutationRecord) => this.domChange.emit(mutation));
    }));
    this.domChange.subscribe((change: MutationRecord) => {
      logger.log(`${change.type.toString()} ` +
        `added: ${change.addedNodes.length} ` +
        `removed: ${change.removedNodes.length}`);
    });
  }

  logChanges(element): Subscription {
    this.changes.observe(element, {
      attributes: true,
      childList: true,
      characterData: true,
      subtree: true
    });

    return this.domChange.subscribe();
  }
}
