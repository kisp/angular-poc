import { TestBed } from '@angular/core/testing';

import { MemoryChartDataProviderService } from './memory-chart-data-provider.service';

describe('MemoryChartDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MemoryChartDataProviderService = TestBed.get(MemoryChartDataProviderService);
    expect(service).toBeTruthy();
  });
});
