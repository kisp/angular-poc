import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangingAsyncArrayPlusCdComponent } from './changing-async-array-plus-cd.component';

describe('ChangingAsyncArrayPlusCdComponent', () => {
  let component: ChangingAsyncArrayPlusCdComponent;
  let fixture: ComponentFixture<ChangingAsyncArrayPlusCdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangingAsyncArrayPlusCdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangingAsyncArrayPlusCdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
