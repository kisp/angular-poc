import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-cd-bound-button',
  templateUrl: './bound-button.component.html',
  styleUrls: ['./bound-button.component.css']
})
export class BoundButtonComponent implements OnInit {

  value: number;

  buttonPressed(value: any) {
    this.value = value;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
