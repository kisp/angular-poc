import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {LoggerService} from '../../component-lifecycle/logger.service';

@Component({
  selector: 'app-query-params',
  templateUrl: './query-params.component.html',
  styleUrls: ['./query-params.component.css']
})
export class QueryParamsComponent implements OnInit {

  constructor(private router: ActivatedRoute, private logger: LoggerService) {
  }

  ngOnInit() {
    this.router.paramMap.subscribe((paramMap: ParamMap) => {
      this.logger.log('paramMap has keys', ...paramMap.keys);
    });
    this.router.queryParams.subscribe(params => {
      for (const k in params) {
        this.logger.log('query param', k, '<=',  params[k]);
      }
    });
  }

}
