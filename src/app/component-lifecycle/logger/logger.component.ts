import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {LoggerService} from '../logger.service';

@Component({
  selector: 'app-logger',
  templateUrl: './logger.component.html',
  styleUrls: ['./logger.component.css']
})
export class LoggerComponent implements OnInit, AfterViewChecked {

  text: string;

  textArr: string[] = [];

  @ViewChild('areaElement', { static: true, read: ElementRef})
  areaElement!: ElementRef;

  constructor(private loggerService: LoggerService) {
  }

  ngOnInit() {
    this.loggerService.getEvents$().subscribe((str: string) => {
      this.textArr.push(str);
      if(this.areaElement)console.log(this.areaElement);
      this.text = this.textArr.join('\n');
    });
  }

  ngAfterViewChecked(): void {
    this.areaElement.nativeElement.scrollTop = this.areaElement.nativeElement.scrollHeight;
  }

}
