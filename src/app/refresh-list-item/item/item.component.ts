import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {IListItem} from "./ListItem";

@Component({
  selector: 'app-list-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input()
  data: IListItem;

  constructor() { }

  ngOnInit() {
  }
}
