import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, OnDestroy} from '@angular/core';
import {LoggerService} from '../../component-lifecycle/logger.service';

@Component({
  selector: 'app-cd-display-count',
  templateUrl: './display-count.component.html',
  styleUrls: ['../style.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayCountComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  value: number;

  tictac = 'Tic';

  count = 0;

  private interval;

  constructor(private ref: ChangeDetectorRef, private logger: LoggerService) {
    this.logger.log('DisplayCountComponent', 'constructor');
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.logger.log('DisplayCountComponent', 'ngOnChanges');
    this.count += 1;
  }

  ngOnInit(): void {
    this.logger.log('DisplayCountComponent', 'ngOnInit');
    this.interval = setInterval(() => {
      this.tictac = this.tictac === 'Tic' ? 'Tac' : 'Tic';
      this.ref.markForCheck();
    }, 2000);
  }

  ngOnDestroy(): void {
    this.logger.log('DisplayCountComponent', 'ngOnDestroy');
    clearInterval(this.interval);
  }

}
