import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoundButtonComponent } from './bound-button.component';

describe('BoundButtonComponent', () => {
  let component: BoundButtonComponent;
  let fixture: ComponentFixture<BoundButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoundButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoundButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
