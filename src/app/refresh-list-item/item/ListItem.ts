export interface IListItem {
  id: symbol;
  name: string;
  text: string;
}

export class ListItem implements IListItem {
  static sequence = 0;

  id: symbol;
  name: string;
  text: string;

  constructor(name: string, text: string) {
    this.id = Symbol();//ListItem.sequence++;
    this.name = name;
    this.text = text;
  }
}
