import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoggerService } from '../component-lifecycle/logger.service';
import { tap } from 'rxjs/operators';


const STRINGS = ['alma', 'korte', 'dio', 'mogyoro', 'mandula', 'sarga-repa'];

const pick = (num: number = 2) => {
  const arr = [];
  for (let i = 0; i < num; i++) {
    const str = STRINGS[Math.floor(Math.random() * STRINGS.length)];
    arr.push(str);
  }
  return arr;
};

@Component({
  selector: 'app-changing-async-array-plus-cd',
  templateUrl: './changing-async-array-plus-cd.component.html',
  styleUrls: ['./changing-async-array-plus-cd.component.css']
})
export class ChangingAsyncArrayPlusCdComponent implements OnInit {
  source: Observable<string[]>;

  constructor(private loggerService: LoggerService) {
  }

  ngOnInit() {
    this.loggerService.log('oninit c-a-arr-cd');
    setInterval(() => {
      this.source = new BehaviorSubject(pick()).pipe(tap(p => this.loggerService.log(p.join(','))));
    }, 1000);
  }
}
