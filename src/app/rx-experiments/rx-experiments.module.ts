import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoadMultipleHttpButtonComponent} from './load-multiple-http-button/load-multiple-http-button.component';
import {LoadSimpleComponent} from './load-simple/load-simple.component';
import {MergeConcatMultipleHttpButtonComponent} from './merge-concat-multiple-http-button/merge-concat-multiple-http-button.component';
import {ButtonsComponent} from './buttons.component';
import {ButtonModule} from "primeng/button";

@NgModule({
  declarations: [
    ButtonsComponent,
    LoadMultipleHttpButtonComponent,
    LoadSimpleComponent,
    MergeConcatMultipleHttpButtonComponent
  ],
  imports: [
    CommonModule,
    ButtonModule
  ]
})
export class RxExperimentsModule { }
