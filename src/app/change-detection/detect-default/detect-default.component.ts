import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-detect-default',
  templateUrl: './detect-default.component.html',
  styleUrls: ['./detect-default.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class DetectDefaultComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
