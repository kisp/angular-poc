var express = require('express');
var app = express();

app.use(function(req,res, next) {
  console.log(req.method, req.path);
  res.set({'Access-Control-Allow-Origin': '*'});
  next();
});

app.get('/all/2', function (req, res) {
  res.send(['http://localhost:3000/example/22', 'http://localhost:3000/example/23']);
});

app.get('/all/1', function (req, res) {
  res.send(['http://localhost:3000/example/12', 'http://localhost:3000/example/13']);
});

app.get('/all', function (req, res) {
  res.send(['http://localhost:3000/example/2', 'http://localhost:3000/example/3']);
});

app.get('/example/:param1', function (req, res) {
  res.send(req.params);
});

app.listen(3000);
