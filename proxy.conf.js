const PROXY_CONFIG = {
  "/api/proxy": {
    "target": "http://localhost:3000",
    "secure": false,
    "bypass": function (req, res, proxyOptions) {
      if (req.headers.accept.indexOf("html") !== -1) {
        console.log("Skipping proxy for browser request.");
        return "/index.html";
      }
      if (req.path.indexOf('conf.js') !== -1) {
        res.json({hello: 'World', 'this': true});
        return;
      }
      req.headers["X-Custom-Header"] = "yes";
    },
    "changeOrigin": true,
    "pathRewrite": {
      "^/api/proxy": ""
    }
  },
  "/https/proxy": {
    "target": "https://index.hu",
    "secure": false,
    "bypass": function (req, res, proxyOptions) {
      console.log(`Proxy: ${req.method} ${req.path}`);
    },
    "changeOrigin": true,
    "pathRewrite": {
      "^/https/proxy": ""
    }
  }
};

module.exports = PROXY_CONFIG;
